/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

console.log("Hello World")

let firstName = prompt("Enter your First name!");
let lastName = prompt("Enter your Last name!");
console.log("Hello," + firstName + lastName);

let myAge = prompt("Enter your Age!");
console.log("Your Age is " + myAge + " years old ");

let myAddress = prompt("Enter your Address!");
alert("Thank you for your input!");
console.log("Your are from " + myAddress);



console.log("My Favorite Music Artist")
function displayMyfavoriteArtist() {
	console.log("1. Franco");
	console.log("2. December Avenue");
	console.log("3. Ebe Dancel");
	console.log("4. Kamikazee");
	console.log("5. Tubero");
}

displayMyfavoriteArtist();


function myFavoriteMovies(){
        console.log("my Favorite Movies are:");
        console.log("1. M3GAN");
        console.log("Rotten Tomatoes ratings: 94%");
        console.log("2. The Banshees of Inisherin");
        console.log("Rotten Tomatoes ratings: 92%");
        console.log("13. Everything Everywhere All at Once");
        console.log("Rotten Tomatoes ratings: 99%");
        console.log("4. Puss in Boots: The Last Wish");
        console.log("Rotten Tomatoes ratings: 91%");
        console.log("5. Shotgun Wedding");
        console.log("Rotten Tomatoes ratings: 93%");

    }

myFavoriteMovies();


printUsers();
function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


